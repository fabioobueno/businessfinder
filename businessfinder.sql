-- --------------------------------------------------------
-- Servidor:                     127.0.0.1
-- Versão do servidor:           10.1.21-MariaDB - mariadb.org binary distribution
-- OS do Servidor:               Win32
-- HeidiSQL Versão:              9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Copiando estrutura do banco de dados para businessfinder
DROP DATABASE IF EXISTS `businessfinder`;
CREATE DATABASE IF NOT EXISTS `businessfinder` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `businessfinder`;

-- Copiando estrutura para tabela businessfinder.categoria
DROP TABLE IF EXISTS `categoria`;
CREATE TABLE IF NOT EXISTS `categoria` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Copiando dados para a tabela businessfinder.categoria: ~7 rows (aproximadamente)
/*!40000 ALTER TABLE `categoria` DISABLE KEYS */;
INSERT INTO `categoria` (`id`, `name`) VALUES
	(1, 'Auto'),
	(2, 'Beauty and Fitness'),
	(3, 'Entertainment'),
	(4, 'Food and Dining'),
	(5, 'Health'),
	(6, 'Sports'),
	(7, 'Travel');
/*!40000 ALTER TABLE `categoria` ENABLE KEYS */;

-- Copiando estrutura para tabela businessfinder.empresa
DROP TABLE IF EXISTS `empresa`;
CREATE TABLE IF NOT EXISTS `empresa` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cep` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `city` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `state` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Copiando dados para a tabela businessfinder.empresa: ~6 rows (aproximadamente)
/*!40000 ALTER TABLE `empresa` DISABLE KEYS */;
INSERT INTO `empresa` (`id`, `title`, `phone`, `address`, `cep`, `city`, `state`, `description`) VALUES
	(3, 'Pizzaria', '(16) 3342 - 6814', 'Rua xxxx n 123, Centro', '14940-000', 'Ibitinga', 'SP', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent nec nibh nec urna aliquet mattis.'),
	(4, 'Club', '(11) 1111-1111', 'Av. YYYY 1234', '17015-021', 'Bauru', 'SP', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent nec nibh nec urna aliquet mattis.'),
	(5, 'XXX Lanches', '(14) 1234-5698', 'Rua ZZZZ 50', '17015-489', 'Bauru', 'SP', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent nec nibh nec urna aliquet mattis.'),
	(6, 'Pizzaria X', '(16) 3342 - 6814', 'Rua xxxx n 123, Centro', '14940-000', 'Ibitinga', 'SP', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent nec nibh nec urna aliquet mattis.'),
	(7, 'XXX Lanches Y', '(14) 1234-5698', 'Rua ZZZZ 50', '17015-489', 'Bauru', 'SP', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent nec nibh nec urna aliquet mattis.'),
	(8, 'Hamburgueria\r\n', '(14) 1234-5698', 'Rua ZZZZ 50', '17015-489', 'Bauru', 'SP', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent nec nibh nec urna aliquet mattis.'),
	(9, 'Club ABC', '(11) 1111-1111', 'Av. YZZZ 123', '17015-021', 'Bauru', 'SP', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent nec nibh nec urna aliquet mattis.'),
	(10, 'Manutenção X', '(11) 123-5489', 'RUA ABC 90', '14940-000', 'Ibitinga', 'SP', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.'),
	(11, 'Bom de Bola', '(11) 1111-2222', 'RUA XXX 29', '17015-011', 'Bauru', 'SP', 'Lorem  consectetur adipiscing elit.'),
	(12, 'Cine ABC', '(99) 8548-9999', 'Rua XPTO 123', '17098-014', 'Bauru', 'SP', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent nec nibh nec urna aliquet mattis.');
/*!40000 ALTER TABLE `empresa` ENABLE KEYS */;

-- Copiando estrutura para tabela businessfinder.empresa_categorias
DROP TABLE IF EXISTS `empresa_categorias`;
CREATE TABLE IF NOT EXISTS `empresa_categorias` (
  `empresa_id` int(11) NOT NULL,
  `categoria_id` int(11) NOT NULL,
  PRIMARY KEY (`empresa_id`,`categoria_id`),
  KEY `categoria_id` (`categoria_id`),
  CONSTRAINT `empresa_categorias_ibfk_1` FOREIGN KEY (`empresa_id`) REFERENCES `empresa` (`id`),
  CONSTRAINT `empresa_categorias_ibfk_2` FOREIGN KEY (`categoria_id`) REFERENCES `categoria` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Copiando dados para a tabela businessfinder.empresa_categorias: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `empresa_categorias` DISABLE KEYS */;
INSERT INTO `empresa_categorias` (`empresa_id`, `categoria_id`) VALUES
	(3, 4),
	(4, 2),
	(4, 3),
	(5, 4),
	(6, 4),
	(7, 4),
	(8, 4),
	(9, 3),
	(10, 1),
	(11, 6),
	(12, 3);
/*!40000 ALTER TABLE `empresa_categorias` ENABLE KEYS */;

-- Copiando estrutura para tabela businessfinder.fos_user
DROP TABLE IF EXISTS `fos_user`;
CREATE TABLE IF NOT EXISTS `fos_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `username_canonical` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `email_canonical` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `salt` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `confirmation_token` varchar(180) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password_requested_at` datetime DEFAULT NULL,
  `roles` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:array)',
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_957A647992FC23A8` (`username_canonical`),
  UNIQUE KEY `UNIQ_957A6479A0D96FBF` (`email_canonical`),
  UNIQUE KEY `UNIQ_957A6479C05FB297` (`confirmation_token`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Copiando dados para a tabela businessfinder.fos_user: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `fos_user` DISABLE KEYS */;
INSERT INTO `fos_user` (`id`, `username`, `username_canonical`, `email`, `email_canonical`, `enabled`, `salt`, `password`, `last_login`, `confirmation_token`, `password_requested_at`, `roles`) VALUES
	(2, 'admin', 'admin', 'fabioo.bueno@gmail.com', 'fabioo.bueno@gmail.com', 1, NULL, '$2y$13$ehN.sfthLGnR0ezoWuduR.AzvggG2PT7SfZ/4D3oK/aS.55cLDb8q', '2017-05-14 23:42:19', NULL, NULL, 'a:1:{i:0;s:10:"ROLE_ADMIN";}');
/*!40000 ALTER TABLE `fos_user` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;

<?php

namespace AppBundle\Form;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type as FormTypes;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class SearchType extends AbstractType {

    const NAME = 'form_pesquisa';

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
//            ->setMethod('GET')
            ->add('pesquisa', FormTypes\SearchType::class, array('label' => false))
            ->add('search', SubmitType::class, array('label' => 'Search'));
    }

    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults([
            'data_class' => null,
            'csrf_protection' => false,
            'method' => 'get'
        ]);
    }

    public function getBlockPrefix() {
        return null;
    }

}
<?php

namespace AppBundle\Form;

use AppBundle\Entity;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type as FormTypes;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class EmpresaType extends AbstractType {

    const NAME = 'business_type';

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
            ->add('title', null, [
                'label' => 'Title'
            ])
            ->add('phone', null, [
                'label' => 'Phone'
            ])
            ->add('address', null, [
                'label' => 'Address'
            ])
            ->add('cep', null, [
                'label' => 'Zip Code'
            ])
            ->add('city', null, [
                'label' => 'City'
            ])
            ->add('state', null, [
                'label' => 'State'
            ])
            ->add('categoria', null, [
                'label' => 'Categories'
            ])
            ->add('description', null, [
                'label' => 'Description',
            ])
            ->add('save', SubmitType::class, array('label' => 'Save'));
    }

    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults([
            'data_class' => Entity\Empresa::class,
        ]);
    }

    public function getBlockPrefix() {
        return self::NAME;
    }
}

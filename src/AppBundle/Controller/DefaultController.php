<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Empresa;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use AppBundle\Form\SearchType;


class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
//        $data = $request->get(SearchType::NAME);

        $data = array(
            'pesquisa' => $request->get('pesquisa')
        );


        $form = $this->createForm(SearchType::class, $data);



        if ($data['pesquisa'] != null) {

            $search = $data['pesquisa'];

            $empresa = $this->getDoctrine()->getRepository('AppBundle:Empresa');

            $query = $empresa->createQueryBuilder('empresa')
                ->select('empresa, categoria')
                ->join('empresa.categoria', 'categoria')
                ->orderBy('empresa.title', 'ASC')
            ;

            $or = $query->expr()->orX();
            $or->add($query->expr()->like('empresa.title', ':likeSearch'))
                ->add($query->expr()->like('empresa.cep', ':likeSearch'))
                ->add($query->expr()->like('empresa.address', ':likeSearch'))
                ->add($query->expr()->like('empresa.city', ':likeSearch'))
                ->add($query->expr()->like('empresa.state', ':likeSearch'))
                ->add($query->expr()->like('empresa.phone', ':likeSearch'))
                ->add($query->expr()->like('categoria.name', ':likeSearch'))
            ;

            $query->andWhere($or)->setParameter('likeSearch', sprintf('%%%s%%',$search));

            $paginator  = $this->get('knp_paginator');
            $pagination = $paginator->paginate($query, $request->query->get('page', 1), 5);

            return $this->render('default/index.html.twig',['form' => $form->createView(),'pagination' => $pagination]);
        }

        return $this->render('default/index.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/business/{id}", requirements={"id"="\d+"}, name="view_company")
     */
    public function viewAction(Empresa $empresa){

        return $this->render('default/view.html.twig', [
            'empresa' => $empresa
        ]);
    }
}

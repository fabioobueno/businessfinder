<?php

namespace AppBundle\Controller\Admin;

use AppBundle\Entity;
use AppBundle\Form\EmpresaType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\Query\Expr\Join;

class IndexController extends Controller
{
    /**
     * @Route("/", name="admin_home")
     */
    public function indexAction(Request $request)
    {
        $empresa = $this->getDoctrine()->getRepository('AppBundle:Empresa');

        $query = $empresa->createQueryBuilder('empresa')
            ->select('empresa, categoria')
            ->join('empresa.categoria', 'categoria')
            ->orderBy('empresa.title', 'ASC')
        ;

        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate($query, $request->query->get('page', 1), 5);


        return $this->render('admin/index.html.twig',['pagination' => $pagination]);
    }


    /**
     * @Route("/add", name="empressa_add")
     * @param Request $request
     * @return Response
     */
    public function newAction(Request $request) {
        $form = $this->createForm(EmpresaType::class, new Entity\Empresa());

        $form->handleRequest($request);

        if ($form->isSubmitted()) {

            if ($form->isValid()) {
                try {
                    $em = $this->getDoctrine()->getManager();
                    $em->persist($form->getData());
                    $em->flush();

                    return $this->redirectToRoute('admin_home');
                } catch (\Exception $e) {
                    $request->getSession()->getFlashBag()->add('error', $e->getMessage());
                }
            } else{
                $request->getSession()->getFlashBag()->add('error', 'Invalid information.');
            }
        }

        return $this->render('admin/empresa_add.html.twig', [
            'form' => $form->createView(),
            'errors'=>''
        ]);
    }
}

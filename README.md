## Instalação
-------------

### 1   Clonar o repositório
```
git clone git@bitbucket.org:fabioobueno/businessfinder.git
```

### 2   Criar virualhost para o projeto
```xml
<VirtualHost *:80>
    ServerName businessfinder.localhost
    DocumentRoot c:/wamp64/www/businessfinder/web
    CustomLog logs/businessfinder.access.log combined
    ErrorLog  logs/businessfinder.error.log
</VirtualHost>
```

### 3   Informar dados de conexão com o banco
```yml
# app/config/parameters.yml
parameters:
    database_host: 127.0.0.1
    database_port: null
    database_name: businessfinder
    database_user: root
    database_password: 
```

### 4   Instalar dependências php
```
composer install
```

### 5   Importar o arquivo SQL (businessfinder.sql)

### 6   User para Login

```
user: admin
pwd: 12345
```
